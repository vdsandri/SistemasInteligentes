import vrep
import sys
import numpy as np
import math
import time
import skfuzzy as fuzz
import skfuzzy.control as ctrl
import pandas as pd
import pdb


################################################################
#                                                              #
#                                                              #
#      Sistema Fuzzy para controle de um robô móvel            #
#                                                              #
#                                                              #
#      Equipe:   Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 25/11/18                    #
#                                                              #
#      Implementação:    class Fuzzy                           #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################


class Fuzzy(object):

    def __init__(self):

        # Para salvar os dados
        self.vec_leftSensor = []
        self.vec_centerLeftSensor = []
        self.vec_centerRightSensor = []
        self.vec_rightSensor = []
        self.vec_leftVel = []
        self.vec_rightVel = []
        self.vec_orientation = []
        self.vec_kjunior_position = []
        self.vec_kjunior_destino = []

        try:

            vrep.simxFinish(-1)
            self.clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)

            if self.clientID != -1:
                print('Connected to remote API server')
            else:
                sys.exit('Could not connect')

            self.returnCode, self.KJuniorHandle = vrep.simxGetObjectHandle(self.clientID, 'KJunior',
                                                                           vrep.simx_opmode_oneshot_wait)

            self.returnCode, self.KJuniorLeftHandle = vrep.simxGetObjectHandle(self.clientID, 'KJunior_motorLeft',
                                                                               vrep.simx_opmode_oneshot_wait)

            self.returnCode, self.KJuniorRightHandle = vrep.simxGetObjectHandle(self.clientID, 'KJunior_motorRight',
                                                                                vrep.simx_opmode_oneshot_wait)

            self.returnCode, self.LaptopHandle = vrep.simxGetObjectHandle(self.clientID, 'laptop',
                                                                          vrep.simx_opmode_oneshot_wait)

            self.centerLeftSensor = ctrl.Antecedent(np.arange(0, 2000, 1), 'centerLeftSensor')
            self.centerRightSensor = ctrl.Antecedent(np.arange(0, 2000, 1), 'centerRightSensor')
            self.leftSensor = ctrl.Antecedent(np.arange(0, 2000, 1), 'leftSensor')
            self.rightSensor = ctrl.Antecedent(np.arange(0, 2000, 1), 'rightSensor')
            self.orientation = ctrl.Antecedent(np.arange(-181, 181, 0.1), 'orientation')
            self.leftWheelVel = ctrl.Consequent(np.arange(-5, 5, 0.1), 'leftWheelVel')
            self.rightWheelVel = ctrl.Consequent(np.arange(-5, 5, 0.1), 'rightWheelVel')

            self.init_parameters()

        except Exception as error:
            pass

    def init_parameters(self):

        self.centerLeftSensor['no-signal'] = fuzz.trapmf(self.centerLeftSensor.universe, [0, 0, 1, 1])
        self.centerLeftSensor['far'] = fuzz.trapmf(self.centerLeftSensor.universe, [1, 2, 900, 1000])
        self.centerLeftSensor['near'] = fuzz.trapmf(self.centerLeftSensor.universe, [900, 1000, 2000, 2000])

        self.centerRightSensor['no-signal'] = fuzz.trapmf(self.centerRightSensor.universe, [0, 0, 1, 1])
        self.centerRightSensor['far'] = fuzz.trapmf(self.centerRightSensor.universe, [1, 2, 900, 1000])
        self.centerRightSensor['near'] = fuzz.trapmf(self.centerRightSensor.universe, [900, 1000, 2000, 2000])

        self.leftSensor['no-signal'] = fuzz.trapmf(self.leftSensor.universe, [0, 0, 1, 1])
        self.leftSensor['far'] = fuzz.trapmf(self.leftSensor.universe, [1, 2, 900, 1000])
        self.leftSensor['near'] = fuzz.trapmf(self.leftSensor.universe, [900, 1000, 2000, 2000])

        self.rightSensor['no-signal'] = fuzz.trapmf(self.rightSensor.universe, [0, 0, 1, 1])
        self.rightSensor['far'] = fuzz.trapmf(self.rightSensor.universe, [1, 2, 900, 1000])
        self.rightSensor['near'] = fuzz.trapmf(self.rightSensor.universe, [900, 1000, 2000, 2000])

        self.orientation['left'] = fuzz.trimf(self.orientation.universe, [-181, -40, 0])
        self.orientation['center'] = fuzz.trapmf(self.orientation.universe, [-60, -20, 20, 60])
        self.orientation['right'] = fuzz.trimf(self.orientation.universe, [0, 40, 181])

        self.leftWheelVel['fastReverse'] = fuzz.trapmf(self.leftWheelVel.universe, [-5, -5, -3.5, -1])
        self.leftWheelVel['slowReverse'] = fuzz.trapmf(self.leftWheelVel.universe, [-4, -1.5, 0, 0])
        self.leftWheelVel['stopped'] = fuzz.trapmf(self.leftWheelVel.universe, [-0.2, -0.1, 0.1, 0.2])
        self.leftWheelVel['slowForward'] = fuzz.trapmf(self.leftWheelVel.universe, [0, 0, 1.5, 4])
        self.leftWheelVel['fastForward'] = fuzz.trapmf(self.leftWheelVel.universe, [1, 3.5, 5, 5])

        self.rightWheelVel['fastReverse'] = fuzz.trapmf(self.rightWheelVel.universe, [-5, -5, -3.5, -1])
        self.rightWheelVel['slowReverse'] = fuzz.trapmf(self.rightWheelVel.universe, [-4, -1.5, 0, 0])
        self.rightWheelVel['stopped'] = fuzz.trapmf(self.rightWheelVel.universe, [-0.2, -0.1, 0.1, 0.2])
        self.rightWheelVel['slowForward'] = fuzz.trapmf(self.rightWheelVel.universe, [0, 0, 1.5, 4])
        self.rightWheelVel['fastForward'] = fuzz.trapmf(self.rightWheelVel.universe, [1, 3.5, 5, 5])

    def wheel_rules(self):

        try:
            wheelRules = [

                ### Original

                ctrl.Rule(self.orientation['left'] & self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'] & self.centerRightSensor[
                    'no-signal'] & self.rightSensor['no-signal'], [self.leftWheelVel['fastForward'], self.rightWheelVel['stopped']]),
                ctrl.Rule(
                    self.orientation['right'] & self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'] & self.centerRightSensor[
                        'no-signal'] & self.rightSensor['no-signal'], [self.leftWheelVel['stopped'], self.rightWheelVel['fastForward']]),
                ctrl.Rule(
                    self.orientation['center'] & self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'] & self.centerRightSensor[
                        'no-signal'] & self.rightSensor['no-signal'],
                    [self.leftWheelVel['fastForward'], self.rightWheelVel['fastForward']]),

                ### Fim do Original

                ## Novas Regras criadas

                ## Left

                ctrl.Rule(
                    self.orientation['left'] & ~self.leftSensor['no-signal'] & ~self.centerLeftSensor['no-signal'] & ~self.centerRightSensor[
                        'no-signal'] & ~self.rightSensor['no-signal'], [self.leftWheelVel['fastReverse'], self.rightWheelVel['stopped']]),
                ctrl.Rule(
                    self.orientation['left'] & ~self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'] & self.centerRightSensor[
                        'no-signal'] & self.rightSensor['no-signal'], [self.leftWheelVel['slowReverse'], self.rightWheelVel['stopped']]),

                ## Right

                ctrl.Rule(
                    self.orientation['right'] & self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'] & ~self.centerRightSensor[
                        'no-signal'] & ~self.rightSensor['no-signal'], [self.rightWheelVel['slowReverse'], self.rightWheelVel['stopped']]),

                ctrl.Rule(
                    self.orientation['center'] & ~self.leftSensor['no-signal'] & ~self.centerLeftSensor['no-signal'] & ~self.centerRightSensor[
                        'no-signal'] & ~self.rightSensor['no-signal'],
                    [self.rightWheelVel['fastReverse'], self.rightWheelVel['fastReverse']]),

                # Decisão
                ctrl.Rule(~self.leftSensor['no-signal'], self.leftWheelVel['fastForward']),
                ctrl.Rule(~self.leftSensor['no-signal'], self.rightWheelVel['slowForward']),

                ctrl.Rule(~self.centerLeftSensor['no-signal'], self.leftWheelVel['fastForward']),
                ctrl.Rule(~self.centerLeftSensor['no-signal'], self.rightWheelVel['stopped']),

                ctrl.Rule(~self.rightSensor['no-signal'], self.rightWheelVel['fastForward']),
                ctrl.Rule(~self.rightSensor['no-signal'], self.leftWheelVel['slowForward']),
                ctrl.Rule(~self.centerRightSensor['no-signal'], self.rightWheelVel['fastForward']),
                ctrl.Rule(~self.centerRightSensor['no-signal'], self.leftWheelVel['stopped']),

                ctrl.Rule(~self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'], self.leftWheelVel['fastForward']),
                ctrl.Rule(~self.leftSensor['no-signal'] & self.centerLeftSensor['no-signal'], self.rightWheelVel['slowForward']),
                ctrl.Rule(~self.rightSensor['no-signal'] & self.centerRightSensor['no-signal'], self.rightWheelVel['fastForward']),
                ctrl.Rule(~self.rightSensor['no-signal'] & self.centerRightSensor['no-signal'], self.leftWheelVel['slowForward']),

                ctrl.Rule(~self.leftSensor['no-signal'] & self.centerLeftSensor['far'], self.leftWheelVel['fastForward']),
                ctrl.Rule(~self.leftSensor['no-signal'] & self.centerLeftSensor['far'], self.rightWheelVel['slowReverse']),
                ctrl.Rule(~self.rightSensor['no-signal'] & self.centerRightSensor['far'], self.rightWheelVel['fastForward']),
                ctrl.Rule(~self.rightSensor['no-signal'] & self.centerRightSensor['far'], self.leftWheelVel['slowReverse']),

                ctrl.Rule(~self.leftSensor['no-signal'] & self.centerLeftSensor['near'], self.leftWheelVel['fastForward']),
                ctrl.Rule(~self.leftSensor['no-signal'] & self.centerLeftSensor['near'], self.rightWheelVel['fastReverse']),
                ctrl.Rule(~self.rightSensor['no-signal'] & self.centerRightSensor['near'], self.rightWheelVel['fastForward']),
                ctrl.Rule(~self.rightSensor['no-signal'] & self.centerRightSensor['near'], self.leftWheelVel['fastReverse']),

                ctrl.Rule(~self.centerLeftSensor['no-signal'] & ~self.centerRightSensor['no-signal'] & ~self.rightSensor['no-signal'],
                          self.leftWheelVel['slowReverse']),
                ctrl.Rule(~self.centerLeftSensor['no-signal'] & ~self.centerRightSensor['no-signal'] & ~self.rightSensor['no-signal'],
                          self.rightWheelVel['fastForward']),

                ctrl.Rule(~self.centerRightSensor['no-signal'] & ~self.centerLeftSensor['no-signal'] & ~self.leftSensor['no-signal'],
                          self.leftWheelVel['fastForward']),
                ctrl.Rule(~self.centerRightSensor['no-signal'] & ~self.centerLeftSensor['no-signal'] & ~self.leftSensor['no-signal'],
                          self.rightWheelVel['slowReverse'])
            ]

            return wheelRules

        except Exception as error:
            pass

    def distance_estimator(self):

        wheel_rules = self.wheel_rules()

        wheelControl = ctrl.ControlSystem(wheel_rules)
        wheelFuzzy = ctrl.ControlSystemSimulation(wheelControl)

        KJuniorProxSensors = []
        KJuniorProxSensorsVal = []

        for i in [1, 2, 4, 5]:
            errorCode, proxSensor = vrep.simxGetObjectHandle(self.clientID, 'KJunior_proxSensor' + str(i),
                                                             vrep.simx_opmode_oneshot_wait)
            KJuniorProxSensors.append(proxSensor)
            errorCode, detectionState, detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                self.clientID, proxSensor, vrep.simx_opmode_streaming)
            distance = round(2000 - np.linalg.norm(detectedPoint) * 20000, 2)
            if distance <= 0 or distance == 2000:
                distance = 0
            KJuniorProxSensorsVal.append(distance)

        # Current KJunior and Objective position
        errorCode, KJuniorCurrentPosition = vrep.simxGetObjectPosition(self.clientID, self.KJuniorHandle, -1,
                                                                       vrep.simx_opmode_streaming)
        errorCode, LaptopPosition = vrep.simxGetObjectPosition(self.clientID, self.LaptopHandle, -1, vrep.simx_opmode_streaming)

        # Orientation related to Origin
        errorCode, _KJuniorOrientation = vrep.simxGetObjectOrientation(self.clientID, self.KJuniorHandle, -1,
                                                                       vrep.simx_opmode_streaming)
        KJuniorOrientationOrigin = [math.degrees(_KJuniorOrientation[0]), math.degrees(_KJuniorOrientation[1]),
                                    math.degrees(_KJuniorOrientation[2])]

        # Orientation related to Objective
        KJuniorOrientationObjective = math.degrees(
            math.atan2(KJuniorCurrentPosition[1] - LaptopPosition[1], KJuniorCurrentPosition[0] - LaptopPosition[0])
        )

        reachedDestination = False

        while not reachedDestination:
            # Current KJunior and Objective position
            errorCode, KJuniorCurrentPosition = vrep.simxGetObjectPosition(self.clientID, self.KJuniorHandle, -1,
                                                                           vrep.simx_opmode_buffer)
            errorCode, LaptopPosition = vrep.simxGetObjectPosition(self.clientID, self.LaptopHandle, -1, vrep.simx_opmode_buffer)

            # Orientation related to X axis
            errorCode, _KJuniorOrientation = vrep.simxGetObjectOrientation(self.clientID, self.KJuniorHandle, -1,
                                                                           vrep.simx_opmode_buffer)
            KJuniorOrientationOrigin = [math.degrees(_KJuniorOrientation[0]), math.degrees(_KJuniorOrientation[1]),
                                        math.degrees(_KJuniorOrientation[2])]
            KJuniorOrientationRelativeToX = 0
            if KJuniorOrientationOrigin[2] >= 0:
                KJuniorOrientationRelativeToX = KJuniorOrientationOrigin[1] * -1
            else:
                if KJuniorOrientationOrigin[1] < 0:
                    KJuniorOrientationRelativeToX = 180 - abs(KJuniorOrientationOrigin[1])
                else:
                    KJuniorOrientationRelativeToX = (180 - KJuniorOrientationOrigin[1]) * -1

            # Orientation related to Objective
            KJuniorOrientationObjective = math.degrees(
                math.atan2(LaptopPosition[1] - KJuniorCurrentPosition[1], LaptopPosition[0] - KJuniorCurrentPosition[0])
            ) * -1

            # Orientation relative to my view and objective
            KJuniorOrientation = KJuniorOrientationRelativeToX - KJuniorOrientationObjective

            ## Coleta de dados kjunior posição

            self.vec_kjunior_position.append(KJuniorOrientationRelativeToX)
            self.vec_kjunior_destino.append(KJuniorOrientationObjective)

            # Read sensors
            KJuniorProxSensorsVal = []
            for i in [1, 2, 4, 5]:
                distance = 0
                errorCode, proxSensor = vrep.simxGetObjectHandle(self.clientID, 'KJunior_proxSensor' + str(i),
                                                                 vrep.simx_opmode_oneshot_wait)
                errorCode, detectionState, detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                    self.clientID, proxSensor, vrep.simx_opmode_buffer)
                # Normalize distance
                # distance = round(2000 - np.linalg.norm(detectedPoint) * 20000, 2)
                distance = round(2000 - np.linalg.norm(detectedPoint) * 20000, 2)

                print(proxSensor, 'read', detectedPoint, 'with distance of', distance, 'detectionState', detectionState,
                      'errorCode', errorCode)
                if detectedObjectHandle == self.LaptopHandle:
                    print('Reached destination!!!!!!!')
                    reachedDestination = True
                if distance <= 0 or distance == 2000 or not detectionState:
                    distance = 0
                KJuniorProxSensorsVal.append(distance)

            # FUZZY

            try:

                self.vec_leftSensor.append(KJuniorProxSensorsVal[0])
                self.vec_centerLeftSensor.append(KJuniorProxSensorsVal[1])
                self.vec_centerRightSensor.append(KJuniorProxSensorsVal[2])
                self.vec_rightSensor.append(KJuniorProxSensorsVal[3])
                self.vec_orientation.append(KJuniorOrientation)

                wheelFuzzy.input['leftSensor'] = KJuniorProxSensorsVal[0]
                wheelFuzzy.input['centerLeftSensor'] = KJuniorProxSensorsVal[1]
                wheelFuzzy.input['centerRightSensor'] = KJuniorProxSensorsVal[2]
                wheelFuzzy.input['rightSensor'] = KJuniorProxSensorsVal[3]
                wheelFuzzy.input['orientation'] = KJuniorOrientation
            except:
                print('ERROR IN INPUTS!!!!!!!!!!!!!!')
                print('ERROR IN INPUTS!!!!!!!!!!!!!!')
                print('ERROR IN INPUTS!!!!!!!!!!!!!!')
                print('ERROR IN INPUTS!!!!!!!!!!!!!!')
                print('ERROR IN INPUTS!!!!!!!!!!!!!!')
                pdb.set_trace()

            try:
                wheelFuzzy.compute()
            except:
                print('ERROR IN COMPUTATION!!!!!!!!!!!!!!')
                print('ERROR IN COMPUTATION!!!!!!!!!!!!!!')
                print('ERROR IN COMPUTATION!!!!!!!!!!!!!!')
                print('ERROR IN COMPUTATION!!!!!!!!!!!!!!')
                print('ERROR IN COMPUTATION!!!!!!!!!!!!!!')
                pdb.set_trace()

            leftVel = wheelFuzzy.output['leftWheelVel']
            rightVel = wheelFuzzy.output['rightWheelVel']

            self.vec_leftVel.append(leftVel)
            self.vec_rightVel.append(rightVel)



            errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorLeftHandle, leftVel,
                                                        vrep.simx_opmode_streaming)
            errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorRightHandle, rightVel,
                                                        vrep.simx_opmode_streaming)

            time.sleep(0.5)

        errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorLeftHandle, 0, vrep.simx_opmode_streaming)
        errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorRightHandle, 0, vrep.simx_opmode_streaming)
        errorCode = vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_oneshot)

    def save_data(self, file_name):

        df = pd.concat([pd.DataFrame(np.asarray(self.vec_kjunior_position, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_orientation, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_leftSensor, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_centerLeftSensor, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_centerRightSensor, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_rightSensor, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_kjunior_destino, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_leftVel, dtype=np.float32)),
                        pd.DataFrame(np.asarray(self.vec_rightVel, dtype=np.float32))], axis=1)

        df.to_csv(file_name, sep='\t', header=False)

