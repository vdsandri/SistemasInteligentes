from Fuzzy import *

################################################################
#                                                              #
#                                                              #
#      Sistema Fuzzy para controle de um robô móvel            #
#                                                              #
#                                                              #
#      Equipe:   Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 25/11/18                    #
#                                                              #
#      Implementação:    main                                  #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################


def main():

    try:
        fz = Fuzzy()
        fz.distance_estimator()
        fz.save_data("data33.csv")

    except (KeyboardInterrupt, SystemExit):
        raise SystemExit('\n! Received keyboard interrupt, quitting threads.\n')


if __name__ == "__main__":
    main()
