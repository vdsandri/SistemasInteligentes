import vrep
import sys
import numpy as np
import math
import time
import pdb
from keras.models import model_from_json
import tensorflow.keras as keras
import tensorflow as tf

################################################################
#                                                              #
#                                                              #
#      Rede Neural para o controle de um robô móvel            #
#                                                              #
#                                                              #
#      Equipe:   Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 26/11/18                    #
#                                                              #
#      Implementação:    class DeepLearning                    #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################


class DeepLearning(object):

    def __init__(self):

        try:

            vrep.simxFinish(-1)
            self.clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)

            if self.clientID != -1:
                print('Connected to remote API server')
            else:
                sys.exit('Could not connect')

            self.returnCode, self.KJuniorHandle = vrep.simxGetObjectHandle(self.clientID, 'KJunior',
                                                                           vrep.simx_opmode_oneshot_wait)

            self.returnCode, self.KJuniorLeftHandle = vrep.simxGetObjectHandle(self.clientID, 'KJunior_motorLeft',
                                                                               vrep.simx_opmode_oneshot_wait)

            self.returnCode, self.KJuniorRightHandle = vrep.simxGetObjectHandle(self.clientID, 'KJunior_motorRight',
                                                                                vrep.simx_opmode_oneshot_wait)

            self.returnCode, self.LaptopHandle = vrep.simxGetObjectHandle(self.clientID, 'laptop',
                                                                          vrep.simx_opmode_oneshot_wait)

            self.model = self.load_file()

        except Exception as error:
            pass

    @staticmethod
    def load_file():

        try:
            json_file = open('model2.json', 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            loaded_model = model_from_json(loaded_model_json)
            # load weights into new model
            loaded_model.load_weights("model2.h5")
            print("Loaded model from disk")
            loaded_model.compile(loss='mse', optimizer='adam')

            return loaded_model

        except IOError as error:
            pass

    def distance_estimator(self):

        KJuniorProxSensors = []
        KJuniorProxSensorsVal = []

        for i in [1, 2, 4, 5]:
            errorCode, proxSensor = vrep.simxGetObjectHandle(self.clientID, 'KJunior_proxSensor' + str(i),
                                                             vrep.simx_opmode_oneshot_wait)
            KJuniorProxSensors.append(proxSensor)
            errorCode, detectionState, detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                self.clientID, proxSensor, vrep.simx_opmode_streaming)
            distance = round(2000 - np.linalg.norm(detectedPoint) * 20000, 2)
            if distance <= 0 or distance == 2000:
                distance = 0
            KJuniorProxSensorsVal.append(distance)

        # Current KJunior and Objective position
        errorCode, KJuniorCurrentPosition = vrep.simxGetObjectPosition(self.clientID, self.KJuniorHandle, -1,
                                                                       vrep.simx_opmode_streaming)
        errorCode, LaptopPosition = vrep.simxGetObjectPosition(self.clientID, self.LaptopHandle, -1, vrep.simx_opmode_streaming)

        # Orientation related to Origin
        errorCode, _KJuniorOrientation = vrep.simxGetObjectOrientation(self.clientID, self.KJuniorHandle, -1,
                                                                       vrep.simx_opmode_streaming)
        KJuniorOrientationOrigin = [math.degrees(_KJuniorOrientation[0]), math.degrees(_KJuniorOrientation[1]),
                                    math.degrees(_KJuniorOrientation[2])]

        # Orientation related to Objective
        KJuniorOrientationObjective = math.degrees(
            math.atan2(KJuniorCurrentPosition[1] - LaptopPosition[1], KJuniorCurrentPosition[0] - LaptopPosition[0])
        )

        reachedDestination = False

        while not reachedDestination:
            # Current KJunior and Objective position
            errorCode, KJuniorCurrentPosition = vrep.simxGetObjectPosition(self.clientID, self.KJuniorHandle, -1,
                                                                           vrep.simx_opmode_buffer)
            errorCode, LaptopPosition = vrep.simxGetObjectPosition(self.clientID, self.LaptopHandle, -1, vrep.simx_opmode_buffer)

            # Orientation related to X axis
            errorCode, _KJuniorOrientation = vrep.simxGetObjectOrientation(self.clientID, self.KJuniorHandle, -1,
                                                                           vrep.simx_opmode_buffer)
            KJuniorOrientationOrigin = [math.degrees(_KJuniorOrientation[0]), math.degrees(_KJuniorOrientation[1]),
                                        math.degrees(_KJuniorOrientation[2])]
            KJuniorOrientationRelativeToX = 0
            if KJuniorOrientationOrigin[2] >= 0:
                KJuniorOrientationRelativeToX = KJuniorOrientationOrigin[1] * -1
            else:
                if KJuniorOrientationOrigin[1] < 0:
                    KJuniorOrientationRelativeToX = 180 - abs(KJuniorOrientationOrigin[1])
                else:
                    KJuniorOrientationRelativeToX = (180 - KJuniorOrientationOrigin[1]) * -1

            # Orientation related to Objective
            KJuniorOrientationObjective = math.degrees(
                math.atan2(LaptopPosition[1] - KJuniorCurrentPosition[1], LaptopPosition[0] - KJuniorCurrentPosition[0])
            ) * -1

            # Orientation relative to my view and objective
            KJuniorOrientation = KJuniorOrientationRelativeToX - KJuniorOrientationObjective

            ## Coleta de dados kjunior posição

            # Read sensors
            KJuniorProxSensorsVal = []
            for i in [1, 2, 4, 5]:
                distance = 0
                errorCode, proxSensor = vrep.simxGetObjectHandle(self.clientID, 'KJunior_proxSensor' + str(i),
                                                                 vrep.simx_opmode_oneshot_wait)
                errorCode, detectionState, detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                    self.clientID, proxSensor, vrep.simx_opmode_buffer)
                # Normalize distance
                # distance = round(2000 - np.linalg.norm(detectedPoint) * 20000, 2)
                distance = round(2000 - np.linalg.norm(detectedPoint) * 20000, 2)

                print(proxSensor, 'read', detectedPoint, 'with distance of', distance, 'detectionState', detectionState,
                      'errorCode', errorCode)
                if detectedObjectHandle == self.LaptopHandle:
                    print('Reached destination!!!!!!!')
                    reachedDestination = True
                if distance <= 0 or distance == 2000 or not detectionState:
                    distance = 0
                KJuniorProxSensorsVal.append(distance)

            # FUZZY

            vec_sensors = []

            try:


                vec_sensors.append(KJuniorOrientationRelativeToX)
                vec_sensors.append(KJuniorOrientation)
                vec_sensors.append(KJuniorProxSensorsVal[0])
                vec_sensors.append(KJuniorProxSensorsVal[1])
                vec_sensors.append(KJuniorProxSensorsVal[2])
                vec_sensors.append(KJuniorProxSensorsVal[3])
                vec_sensors.append(KJuniorOrientationObjective)

            except:
                print('ERROR IN INPUTS!!!!!!!!!!!!!!')
                pdb.set_trace()

            pred = self.model.predict(np.array([vec_sensors]))

            pred = pred.tolist()
            leftVel = pred[0][0]
            rightVel = pred[0][1]

            errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorLeftHandle, leftVel,
                                                        vrep.simx_opmode_streaming)
            errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorRightHandle, rightVel,
                                                        vrep.simx_opmode_streaming)

            time.sleep(0.5)

        errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorLeftHandle, 0, vrep.simx_opmode_streaming)
        errorCode = vrep.simxSetJointTargetVelocity(self.clientID, self.KJuniorRightHandle, 0, vrep.simx_opmode_streaming)
        errorCode = vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_oneshot)

