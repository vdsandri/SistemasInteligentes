from DeepLearning import *

################################################################
#                                                              #
#                                                              #
#      Rede Neural para o controle de um robô móvel            #
#                                                              #
#                                                              #
#      Equipe:   Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 26/11/18                    #
#                                                              #
#      Implementação:    main                                  #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################


def main():

    try:
        ne = DeepLearning()

        ne.distance_estimator()

    except (KeyboardInterrupt, SystemExit):
        raise SystemExit('\n! Received keyboard interrupt, quitting threads.\n')


if __name__ == "__main__":
    main()
