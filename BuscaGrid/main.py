

################################################################
#                                                              #
#                                                              #
#      Algoritmos de Busca no GRID - Sistemas Inteligentes     #
#                                                              #
#                                                              #
#      Equipe:   Steven Justen Tunnicliff                      #
#                Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 04/09/18                    #
#                                                              #
#      Implementação:    main                                  #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################

from Environment import *
from Agent import *
import sys
import signal
import os


# O número 8 é a posição atual do agente na matrix
# O número 9 é a posição final


def signal_handler(signal, frame):
    print('\nStop: pressing the CTRL+C!')
    sys.exit(0)


def clear():

    os.system('cls' if os.name == 'nt' else 'clear')


def position(args):

    position = list()
    msg = ""

    if args == 1:
        msg = "Posição inicial"
        flag = True

    elif args == 2:
        msg = "Posição final"
        flag = True

    else:
        flag = False

    while flag:

        entrada = input(msg + " do eixo x (0<= e <=9): ")
        position.append(int(entrada))
        if 0 <= int(entrada) <= 9:

            while True:
                entrada = input(msg + " do eixo y (0<= e <=9): ")

                if 0 <= int(entrada) <= 9:
                    position.append(int(entrada))
                    return position

                else:
                    print("\nValor incorreto para o eixo y do grid")
        else:
            print("\nValor incorreto para o eixo x do grid")


def chooseAlgorith(t_agent):

    enter = input("Digite: 1 - BFS, 2 - A*: ")

    if int(enter) == 1:
        queue = t_agent.breadthFirstSearch()

    elif int(enter) ==2:
        queue = t_agent.aStar()

    else:
        queue = 0

    clear()
    return queue


def main():

    # Para usar o ctr c
    signal.signal(signal.SIGINT, signal_handler)

    # Criando o Ambiente
    env = Environment()
    env.buildMatrix(10, 10)
    env.addwallDefault()

    # Criando o agente
    agt = Agent()

    initial_position = position(1)

    if len(initial_position) == 2:
        final_position = position(2)

        if len(final_position) == 2:

            agt.buildAgent(0, [initial_position[0], initial_position[1]], env.getMatrixGrid())
            agt.setGoals([final_position[0], final_position[1]])

            # Inserindo a posição inicial
            env.addPosition(initial_position[0], initial_position[1], 8)

            # Inserindo a posição final
            env.addPosition(final_position[0], final_position[1], 9)

            # Escolhendo o algoritmo de busca
            queue = chooseAlgorith(agt)

            # Imprimindo o Grid
            env.printMatrix()

            print("queue size: " + str(len(queue)))
            print("queue solution: " + str(queue))

    else:
        position(1)


if __name__ == "__main__":

    main()
