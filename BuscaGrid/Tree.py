

################################################################
#                                                              #
#                                                              #
#      Algoritmos de Busca no GRID - Sistemas Inteligentes     #
#                                                              #
#                                                              #
#      Equipe:   Steven Justen Tunnicliff                      #
#                Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 04/09/18                    #
#                                                              #
#      Implementação:    Tree                                  #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################


class Tree:

    node = []
    visited = []
    border = []

    def insertNodes(self, t_node):
        self.node.append(t_node)

    def insertBorder(self, t_node):
        self.border.append(t_node)

    def searchTreeBorder(self, t_position):

        if t_position in (x.position for x in self.border):
            return True

        else:
            return False

    def sortBorder(self):
        self.border.sort(key=lambda node: node.weighted)


class Node:

    father = []
    children = []
    position = []
    action = []

    def __init__(self,  t_father, t_children, t_action, t_pos):

        self.father = t_father
        self.children = t_children
        self.action = t_action
        self.position = t_pos

        # Inicializando o custo
        self.cost = 0       # Total do inicio até o ponto
        self.distance = 0   # Estimatimativa de distancia do ponto até o objetivo

        # Soma do custo e da distancia
        self.weighted = self.cost + self.distance

    def setCost(self, t_cost, t_distance):

        self.cost = t_cost
        self.distance = t_distance
        self.weighted = self.cost + self.distance
